Introduction
===============

SSHLibrary is a Robot Framework test library for SSH and SFTP.
It is operating system independent and supports 
Python 2.7 as well as Python 3.4 or newer. It supports
multiple connections to different hosts

Build and Installation instructions
======================================

To build the Debian package in SDK, use the below command 
and ensure that the package meets all build dependencies.

   $ dpkg-buildpackage -us -uc

After building SSHLibrary source package, binary package (.deb)
file is generated which can be installed in 
SDK by using the below command:

   $ sudo dpkg -i <binary packge>

Once the above command is executed on terminal, 
the binary package is installed in SDK.

Usage
======

To use SSHLibrary in Robot Framework tests, 
the library needs to first be imported using the 
Library setting as any other library.

A simple example is written using SSHLibrary.
New connections are opened with Open Connection keyword.
Login into the host is done with username and password (Login).

Example:

*** Settings ***
Library     SSHLibrary

*** Variables ***

${HOST}         10.168.128.184
${HOST_USERNAME}    user
${HOST_PASSWORD}    user


*** Test Cases ***

${id}=   Open Connection     host=${HOST}    alias=s1
${output}=   Login           ${HOST_USERNAME}    ${HOST_PASSWORD}
${sid}=   Execute Command   echo "Hello"
Close Connection

Licensing
=========

SSHLibrary is licensed under the Apache License 2.0


